"""
Wzór na BMI to: masa ciała / wzrost**2
tu można wpisać co sie chce
bo dzięki trzem cudzysłowiom\nmożna pisać tyle wierszy ile się chce
"""
news1 = """
Podaj swoją wagę w [kg].
Twoja waga to: """
news2 = """
Podaj swój wzrost w [cm].
Twój wzrost to: """

weight = int(input(news1))
height_cm = int(input(news2))
height = height_cm / 100.0
bmi = weight / height ** 2

print(f"\nTwoje BMI dla wagi {weight} i wzrostu {height_cm} wynosi {round(bmi,2)}\n")
