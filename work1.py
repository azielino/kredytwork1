"""
program do pokazania zmian w trakcie splacania kredytu
za okres 24 miesiecy
"""

print("_" * 90)

loan = float(input("|   Podaj wysokosc kredytu: "))
perc = float(input("|   Podaj oprocentownie kredytu w procentach wieksze niz 2,5% : "))
loanPay = float(input("|   Podaj wysokosc miesecznej raty kredytu: "))

print("^" * 90)

Infl1 = 1.59282448436825
Infl2 = -0.453509101198007
Infl3 = 2.32467171712441
Infl4 = 1.26125440724877
Infl5 = 1.78252628571251
Infl6 = 2.32938454145522
Infl7 = 1.50222984223283
Infl8 = 1.78252628571251
Infl9 = 2.32884899407637
Infl10 = 0.616921348207244
Infl11 = 2.35229588637833
Infl12= 0.337779545187098
Infl13 = 1.57703524727525
Infl14 = -0.292781442607648
Infl15 = 2.48619659017508
Infl16 = 0.267110317834564
Infl17 = 1.41795267229799
Infl18 = 1.05424326726375
Infl19 = 1.4805201044812
Infl20 = 1.57703524727525
Infl21 = -0.0774206903147018
Infl22 = 1.16573339872354
Infl23 = -0.404186717638335
Infl24 = 1.49970852083123

loan1 = (1 + ((Infl1+perc)/loan)) * loan - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan1, 2)}, to {round((loan - loan1), 2)} mniej niż w poprzednim miesiącu.")
loan2 = (1 + ((Infl2+perc)/loan)) * loan1 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan2, 2)}, to {round((loan1 - loan2), 2)} mniej niż w poprzednim miesiącu.")
loan3 = (1 + ((Infl3+perc)/loan)) * loan2 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan3, 2)}, to {round((loan2 - loan3), 2)} mniej niż w poprzednim miesiącu.")
loan4 = (1 + ((Infl4+perc)/loan)) * loan3 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan4, 2)}, to {round((loan3 - loan4), 2)} mniej niż w poprzednim miesiącu.")
loan5 = (1 + ((Infl5+perc)/loan)) * loan4 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan5, 2)}, to {round((loan4 - loan5), 2)} mniej niż w poprzednim miesiącu.")
loan6 = (1 + ((Infl6+perc)/loan)) * loan5 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan6, 2)}, to {round((loan5 - loan6), 2)} mniej niż w poprzednim miesiącu.")
loan7 = (1 + ((Infl7+perc)/loan)) * loan6 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan7, 2)}, to {round((loan6 - loan7), 2)} mniej niż w poprzednim miesiącu.")
loan8 = (1 + ((Infl8+perc)/loan)) * loan7 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan8, 2)}, to {round((loan7 - loan8), 2)} mniej niż w poprzednim miesiącu.")
loan9 = (1 + ((Infl9+perc)/loan)) * loan8 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan9, 2)}, to {round((loan8 - loan9), 2)} mniej niż w poprzednim miesiącu.")
loan10 = (1 + ((Infl10+perc)/loan)) * loan9 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan10, 2)}, to {round((loan9 - loan10), 2)} mniej niż w poprzednim miesiącu.")
loan11 = (1 + ((Infl11+perc)/loan)) * loan10 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan11, 2)}, to {round((loan10 - loan11), 2)} mniej niż w poprzednim miesiącu.")
loan12 = (1 + ((Infl12+perc)/loan)) * loan11 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan12, 2)}, to {round((loan11 - loan12), 2)} mniej niż w poprzednim miesiącu.")
loan13 = (1 + ((Infl13+perc)/loan)) * loan12 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan13, 2)}, to {round((loan12 - loan13), 2)} mniej niż w poprzednim miesiącu.")
loan14 = (1 + ((Infl14+perc)/loan)) * loan13 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan14, 2)}, to {round((loan13 - loan14), 2)} mniej niż w poprzednim miesiącu.")
loan15 = (1 + ((Infl15+perc)/loan)) * loan14 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan15, 2)}, to {round((loan14 - loan15), 2)} mniej niż w poprzednim miesiącu.")
loan16 = (1 + ((Infl16+perc)/loan)) * loan15 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan16, 2)}, to {round((loan15 - loan16), 2)} mniej niż w poprzednim miesiącu.")
loan17 = (1 + ((Infl17+perc)/loan)) * loan16 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan17, 2)}, to {round((loan16 - loan17), 2)} mniej niż w poprzednim miesiącu.")
loan18 = (1 + ((Infl18+perc)/loan)) * loan17 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan18, 2)}, to {round((loan17 - loan18), 2)} mniej niż w poprzednim miesiącu.")
loan19 = (1 + ((Infl19+perc)/loan)) * loan18 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan19, 2)}, to {round((loan18 - loan19), 2)} mniej niż w poprzednim miesiącu.")
loan20 = (1 + ((Infl20+perc)/loan)) * loan19 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan20, 2)}, to {round((loan19 - loan20), 2)} mniej niż w poprzednim miesiącu.")
loan21 = (1 + ((Infl21+perc)/loan)) * loan20 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan21, 2)}, to {round((loan20 - loan21), 2)} mniej niż w poprzednim miesiącu.")
loan22 = (1 + ((Infl22+perc)/loan)) * loan21 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan22, 2)}, to {round((loan21 - loan22), 2)} mniej niż w poprzednim miesiącu.")
loan23 = (1 + ((Infl23+perc)/loan)) * loan22 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan23, 2)}, to {round((loan22 - loan23), 2)} mniej niż w poprzednim miesiącu.")
loan24 = (1 + ((Infl24+perc)/loan)) * loan23 - loanPay
print(f"Twoja pozostała kwota kredytu to {round(loan24, 2)}, to {round((loan23 - loan24), 2)} mniej niż w poprzednim miesiącu.")

print("*" * 90)